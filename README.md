# 2022-02-enterprise-demo


Simple demo for Anchore Enterprise, including Jenkins and GitLab CI  workflow examples.

DEPRECATED, take a look at this repo instead:
https://gitlab.com/pvn_test_images/2022-08-enterprise-demo
